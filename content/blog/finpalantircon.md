---
title: ¡Se clausura la PalantirCon! ¿Dónde están las grabaciones?
brief: 
image: /images/palantirconbanner.jpg
date: 2020-10-13
draft: false
---

Con sentimientos encontrados, tenemos que despedirnos: con la alegría del tiempo compartido alrededor de la figura del Profesor y su obra, con la tristeza del regreso y la añoranza de no poder compartir en persona una buena pinta, debemos decir adiós. El último día fue emocionante, regado con lágrimas que no fueron amargas. Aquí tenéis, como no podía ser de otra forma, el enlace a la lista de reproducción que incluye todos los bloques de los cuatro días.

https://www.youtube.com/playlist?list=PLrVeBpvB2kh_6BQ-iRzhVIjCSnDbdXudF

Os sugerimos que tengáis el [Horario](/schedule) a mano para elegir los vídeos de la playlist y poder buscar la charla que os interesa.

