---
title: ¡400 inscripciones en apenas 10 días!
brief: 
image: /images/palantirconbanner.jpg
date: 2020-09-30
draft: false
---

En el equipo de organización aún cunde el desconcierto ¿Cómo? ¿400 inscripciones ya? Efectivamente, tolkiendili de todos los rincones del mundo han ido sumándose a este evento surgido de la pura necesidad de la Sociedad Tolkien Española de contar con un evento en el que reunirnos en 2020, invitando a simpatizantes y amantes de la obra de JRR Tolkien en general.

Más de 30 actividades que incluyen charlas, mesas redondas, talleres, partidas, teatrillos, concursos y otras fórmulas de difícil clasificación **que se emitirán en directo por [streaming](/streaming)**. Quedan menos de dos semanas para disfrutar de un horario repartido en 4 días, del 9 al 12 de octubre, y nos gustaría contar con todo el mundo posible. ¿Sabéis que estamos cerca de lograr un récord de participación en un congreso online sobre JRR Tolkien? Si lo conseguimos, ¡lo anunciaremos debidamente! Mientras tanto, ayúdanos a difundir compartiendo en redes sociales este evento a través de nuestra web y el [formulario de inscripción]((https://forms.gle/wunaG28EcwcvsrE36)).

Está claro que a un lado u otro del atlántico *y más allá del oeste*, mucha gente se ha sentido atraída por la PalantirCon, una iniciativa de un grupo de entusiastas tolkiendili de la STE en la que van a participar directamente frente a las *web-cámaras* más de 40 personas diferentes para compartir su conocimiento, entreternos y llevarnos más cerca de la Tierra Media. Todo regado con mucho buen humor y generosas dosis de fraternidad.

Recuerda [inscribirte en la PalantirCon](https://forms.gle/wunaG28EcwcvsrE36) y visitar el [horario de actividades](/sessions) por si algunas requieren una inscripción adicional.

¡Te esperamos!
Comisión Organizadora de la Meren Palantírion