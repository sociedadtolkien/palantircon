---
title: ¡Publicamos horario definitivo!
brief: 
image: /images/palantirconbanner.jpg
date: 2020-10-04
draft: false
---

Estamos en la recta final de la Meren Palantírion y poco queda ya sin confirmar.

Desde hacía una semana habíamos publicado el cartel definitivo de ponentes y actividad, esast últimas en su orden correspondiente.

Una vez confirmado este orden, hemos creado una nueva sección, llamada [Horario](/schedule) en donde se muestran los cuatro días a modo de cuadrante y así visualizar más cómodamente lo que sucede en cada momento.

Recuerda [inscribirte en la PalantirCon](https://forms.gle/wunaG28EcwcvsrE36) y visitar el [listado de actividades](/sessions) por si algunas requieren una inscripción adicional.


¡Te esperamos!
Comisión Organizadora de la Meren Palantírion