---
title: ¡Inscripciones cerradas!
brief: 
image: /images/palantirconbanner.jpg
date: 2020-10-09
draft: false
---

¡Se cierran las inscripciones! Con más de 630 personas inscritas vamos a disfrutar de un evento tolkieniano sin precedentes.

¡Nos vemos mañana viernes a las 17h! 

Asegúrate de ponerte un recordatorio para [el primer streaming en vivo](https://www.youtube.com/watch?v=MYBpcRzbRVA).

Mirad vuestros buzones de correo para las instrucciones. Revisad la carpeta de SPAM por si acaso :)

Tenna rato!
