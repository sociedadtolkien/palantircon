---
title: ¡Lanzamos la web de la PalantirCon!
brief: 
image: /images/palantirconbanner.jpg
date: 2020-09-20
draft: false
---

Tras el trabajo de estas últimas semanas, la página web de la Meren Palantírion por fin está lista para ser lanzada. ¡Aquí la tenéis!

En ella encontramos varias secciones que serán muy útiles para quienes deseen acudir a las charlas y actividades que se ofrecen en el evento. En el menú principal, encontramos el enlace al [formulario de inscripción](https://docs.google.com/forms/d/e/1FAIpQLSeF30aZuuklmsyGT5tJA5dRVH4yJkEHdBiHavJWWwZOUPcy6Q/viewform) para las personas que asistan:

{{< centered-img src="/images/enlace-inscripcion.png" alt="Imagen de enlace a inscripción" width="800" >}}

En la web podemos econtrar [una sección de preguntas frecuentes (FAQs)](/faqs) dirigida a responder las dudas que puedan surgir con respecto al evento. Se recogen preguntas generales sobre qué es la PalantirCon, fechas y horarios aproximados, medio en que se realiza el evento (una plataforma online), los requisitos logísticos para poder participar en las distintas actividades o cómo realizar propuestas.

{{< centered-img src="/images/FAQs-PC-1024x441.png" alt="Captura de la sección de FAQs" width="800" >}}

Además, también cuenta con una sección en la que podemos ver [qué charlas y talleres hay programados](/sessions), incluyendo sus horarios, ponentes y un pequeño resumen de las actividades. De momento no podremos ver los horarios y listado de ponentes definitivo, que se actualizará en los próximos días.

{{< centered-img src="/images/Charlas-PC-1024x468.png" alt="Captura de la sección de Actividades" width="800" >}}

Si queréis estar al tanto de todas las novedades de la PalantirCon, podéis hacerlo en sus redes sociales, [Facebook](https://facebook.com/merenpalantirion), [Twitter](https://twitter.com/PalantirCon) e [Instagram](https://instagram.com/merenpalantirion), y desde hoy también en nuestra nueva página web.

Estamos deseando contar con todas vuestras ponencias y asistencia en esta nueva aventura, ¡uníos a la PalantirCon!
