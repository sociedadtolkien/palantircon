---
title: Preparando la I Meren Palantírion – La PalantirCon
brief: 
image: /images/palantirconbanner.jpg
date: 2020-08-06
draft: false
---

El 2020 está siendo un un año de lo más inesperado. La pandemia mundial del Covid-19 es algo con lo que nunca hubiéramos creído encontrarnos al cruzar nuestra puerta, pero al igual que a los integrantes de la Compañía del Anillo, no nos queda más remedio que intentar sortear los peligros y acometer los desafíos de estos tiempos de la mejor manera posible.

Con mucho pesar, el tan esperado evento de la XXV Mereth Aderthad o EstelCon, ha tenido que ser suspendido en esta edición tras una dura decisión conjunta por parte de la organización del mismo y con la Comisión Permanente de la Sociedad Tolkien Española. El vacío que deja esta celebración anual hasta su próxima realización nos ha hecho pensar: ¿y si pudiéramos vernos todos, charlar, escuchar conferencias y hacer talleres durante esos días desde nuestros hogares? En respuesta a esta pregunta surge la I Meren Palantírion o PalantirCon.

{{< centered-img src="/images/palantirconbanner.jpg" alt="Banner de la PalantirCon" width="800" >}}

La Meren Palantírion (quenya para Festival de las Piedras Videntes) se concibe como unas jornadas en línea para poder disfrutar de la obra de Tolkien en casa, en las que se llevarán a cabo talleres, charlas, simposios, juegos y mesas redondas entre otras actividades. Con esta iniciativa queremos llevar el espíritu del Profesor a las plataformas online ya que no podemos reunirnos físicamente, iniciando la andadura de un tipo de convención que no se había realizado hasta ahora en la STE.

Cualquier persona que quiera proponer una actividad, sumarse al grupo de trabajo que está organizando el evento o para cualquier consulta, puede ponerse en contacto a través del correo electrónico: merenpalantirion@gmail.com

El evento tendrá lugar los días 9,10,11 y 12 de octubre. Será abierto y gratuito para todas aquellas personas que quieran escuchar las charlas o simposios, y en el caso de actividades que puedan contar con un aforo limitado (como juegos de rol) se indicará más adelante en la página web y las redes sociales del mismo: Facebook, Twitter e Instagram.

{{< centered-img src="/images/aragorngimlilegolas.jpg" alt="Banner de la PalantirCon" width="800" >}}

Estamos deseando contar con todas vuestras ideas para este nuevo proyecto, ¡preparaos para la PalantirCon!