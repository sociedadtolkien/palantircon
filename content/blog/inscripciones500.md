---
title: ¡500 inscripciones a 5 días de la PalantirCon!
brief: 
image: /images/palantirconbanner.jpg
date: 2020-10-05
draft: false
---

Con cierto vértigo pero también muchísima ilusión vamos viendo como la PalantirCon sigue recibiendo inscripciones de todas partes del mundo.

Ya somos 500 personas inscritas para disfrutar de un congreso online para celebrar la obra de JRR Tolkien desde la comodidad de nuestras casas pero sin perder todas las oportunidades para conectarnos con personas afines. Hemos dispuesto una serie de salas virtuales diseñadas específicamente para tener un espacio en paralelo de charla informal. Son las salas denominadas "Una reunión inesperada" y tendremos varias dispuestas a la vez.

Hemos actualizado las secciones de [Preguntas Frecuentes](/faq) y la de [Streaming](/streaming). Os rogamos que leáis su contenido, está lleno de información práctica.

Recuerda [inscribirte en la PalantirCon](https://forms.gle/wunaG28EcwcvsrE36) y visitar el [horario de actividades](/sessions) por si algunas requieren una inscripción adicional.

Lo único que falta por publicar es el horario de actividades en formato cuadrante para facilitar la vista por días y bloques de mañana y tarde, estará listo en breve :)

¡Te esperamos!
Comisión Organizadora de la Meren Palantírion