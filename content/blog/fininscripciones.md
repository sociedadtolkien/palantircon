---
title: Hoy jueves 8 a las 24h finaliza el proceso de inscripción
brief: 
image: /images/palantirconbanner.jpg
date: 2020-10-08
draft: false
---

Tensad vuestros arcos, preparad vuestras plumas, afinad el oído y desempolvad vuestras mejores galas… ¡Ya esta aquí la Meren Palantírion!

Tenemos muchas ganas de arrancar motores, y con cierto vértigo pero también muchísima ilusión vamos viendo como la PalantirCon sigue recibiendo inscripciones de todas partes del mundo. Y es que ya somos casi 600 personas inscritas para disfrutar de un congreso online para celebrar la obra de JRR Tolkien desde la comodidad de nuestras casas pero sin perder todas las oportunidades para conectarnos con personas afines.

Se han actualizado las secciones de [Preguntas Frecuentes](/faq) y la de [Streaming](/streaming) de la página web y os recomendamos que leáis su contenido, ya que está lleno de información práctica.

Debido a la calurosa acogida que el evento está teniendo, y como no queremos dejarnos nada en el tintero en el último minuto, os informaros de que las inscripciones a la PalantirCon se cerrarán hoy jueves 8 de octubre a las 24:00 h (hora central europea).

Esperamos que todo el mundo que esté interesado pueda asistir y estamos deseando compartir esta aventura tan inesperada, así que daos prisa e [inscribíos](https://forms.gle/wunaG28EcwcvsrE36) en el evento antes de que las últimas luces del Día de Dúrin desaparezcan. ¡Uníos a la PalantirCon!
