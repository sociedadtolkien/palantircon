---
title: ¡Saludos a América Latina!
brief: 
image: /images/palantirconbanner.jpg
date: 2020-09-28
draft: false
---

*«Estas piedras eran un regalo de los Eldar a Amandil, padre de Elendil, para consuelo de los fieles de Númenor en los días de oscuridad, cuando los Elfos no podían ir ya a esa tierra bajo la sombra de Sauron. Se llamaban las Palantíri, las que vigilan desde lejos»*

*J. R.R. Tolkien, El Silmarillion*

En estos tiempos en los que debemos vivir bajo la sombra del miedo, las Palantíri no solo son consuelo y esperanza, también una oportunidad de unirnos más con compañeros castellanoparlantes allende los mares. 

Es por eso que hemos intentado que la PalantirCon tenga un horario en el que podamos unirnos con nuestros compañeros y compañeras de América. Esperamos y deseamos que todos los seguidores y seguidoras de la obra de Tolkien puedan usar estar Palantíri para conectar y disfrutar pues para eso fueron ideadas por el autor originalmente 

La gran mayoría de charlas comienzan a las 17 horas (Hora España) lo que se traduce en una diferencia horaria de 5 horas con Argentina, Chile, Uruguay, 6 horas con Bolivia, Paraguay, Puerto Rico o Venezuela; 7 horas con Méjico, Ecuador, Perú, Colombia, Florida, Cuba, República Dominicana o Nueva York. Eso significa que las actividades de la PalantirCon que sean por la tarde allí serán por la mañana para que puedan disfrutarse desde ambos lados del charco. 

Mandamos invitaciones a las Sociedades Tolkien de América Latina y hemos tenido una gran respuesta pero no hace falta estar en una asociación para disfrutar del evento. Queremos que todos los tolkiendili puedan disfrutar de esta gran fiesta virtual. 

Si lo que quieres es tener más información sobre esta convención, hemos preparado una web bastante completa en la que podrás enterarte  de las últimas novedades, ver el horario, consultar quiénes son los ponentes o ver qué actividades son interesantes para organizarte en un fin de semana que esperamos que sea inolvidable. 

Para hacernos una idea de cuánta gente tiene interés en el evento y darles así la mejor cobertura y utilizar los medios más apropiados hemos preparado un pequeño formulario para que la gente que desee acudir nos lo pueda hacer saber. 

Con gran ilusión estaremos contemplando nuestras Palantíri esperando las respuestas de los cuatro puntos cardinales de Arda. En octubre tenemos una gran cita a la que no podemos faltar. "Que el viento bajo las alas os sostenga allá donde el sol navega y la luna camina".

Comisión organizadora de la Meren Palantírion.

{{< centered-img src="/images/blog/esquemahorario.jpg" alt="Esquema de horario con zonas horarias" width="800" >}}
**Esquema de horario con zonas horarias**
