---
title: ¡Únete a la PalantirCon, la I Meren Palantírion!
brief: ¡Buscamos actividades!
image: /images/palantirconbanner.jpg
date: 2020-08-28
draft: false
---


» Es evidente que yo volví de mi viaje por un camino demasiado recto. Gandalf hubiera podido pasearme un poco más. Pero entonces la subasta habría terminado antes que yo volviera, y entonces habría tenido más contratiempos aún. De todos modos ahora es demasiado tarde; y la verdad es que creo que es mucho más cómodo estar sentado aquí y oír todo lo que pasó. El fuego es muy acogedor aquí, y la comida es muy buena, y hay elfos si quieres verlos. ¿Qué más puedes pedir? «

Tolkien, J.R.R, El Señor de los Anillos, Ilustrado por Alan Lee, Barcelona, Editorial Planeta, 1993, p. 1060

Como sabiamente Bilbo Bolsón comenta en este párrafo, en algunas ocasiones es más cómodo (y seguro) escuchar las historias sobre aventuras y lejanos parajes desde el calor del hogar. Y como bien sabe su querido sobrino Frodo, a veces no podemos elegir qué eventos sucederán en nuestra época, pero sí podemos decidir qué haremos con el tiempo que nos dieron.

La Sociedad Tolkien Española, entre sus diferentes eventos, organiza la Mereth Aderthad o EstelCon, el encuentro anual entre todos los miembros de la misma y abierto a cualquier persona que quiera acudir, una Fiesta de la Reunión de varios días de duración llena de conferencias, talleres, música, juegos, lecturas… Sin embargo, debido a las circunstancias del tiempo en que vivimos dada la actual pandemia del Covid-19, el tan esperado evento de la XXV Mereth Aderthad (que tendría lugar del 9 al 12 de octubre) ha sido suspendido en esta edición. Tras esta dura decisión, el vacío que deja esta celebración anual hasta su próxima realización ha dado paso a una propuesta paraque los socios de la STE y para cualquier persona que quiera asistir pueda hacerlo desde su hogar: la Meren Palantírion o PalantirCon.

La Meren Palantírion (quenya para Festival de las Piedras Videntes) se concibe como unas jornadas en línea para poder disfrutar de la obra de Tolkien en casa, en las que se llevarán a cabo talleres, charlas, simposios, juegos y mesas redondas entre otras actividades. Con esta iniciativa queremos llevar el espíritu del Profesor a las plataformas online ya que no podemos reunirnos físicamente, y aprovechar esta ocasión para invitar a toda aquella persona que quiera unirse al festival.

{{< centered-img src="/images/gandalf_saruman.jpg" alt="Banner de la PalantirCon" width="800" >}}

Cualquier persona que quiera proponer una actividad, sin que sea necesario formar parte de la STE, sumarse al grupo de trabajo que está organizando el evento o que quiera realizar cualquier consulta, puede ponerse en contacto a través del correo electrónico: merenpalantirion@gmail.com

El evento tendrá lugar los días 9,10,11 y 12 de octubre. Será abierto y gratuito para todas aquellas personas que quieran escuchar las charlas o simposios, y en el caso de actividades que puedan contar con un aforo limitado (como juegos de rol) se indicará más adelante en la página web y las redes sociales del mismo: Facebook, Twitter e Instagram.

Estamos deseando contar con todas vuestras ideas para este nuevo proyecto, ¡uníos a la PalantirCon!