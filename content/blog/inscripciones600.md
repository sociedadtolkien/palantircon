---
title: ¡600 inscripciones a horas del cierre de inscripciones!
brief: 
image: /images/palantirconbanner.jpg
date: 2020-10-08
draft: false
---

Hoy a medianoche se cierra el proceso de inscripción de la PalantirCon que arranca mañana a las 17h y ¡hemos superado las 600 inscripciones!

600 personas que en parte o todo el evento estarán participando de 4 días de actividades de lo más variopinto alrededor de la obra de JRR Tolkien. Un evento de Tolkien en castellano que ya podemos afirmar que es el de mayor audiencia hasta la fecha en cualquier idioma.

Os compartimos nuestra inmensa alegría mientras seguimos trabajando duro para que resulte una experiencia de lo más enriquecedora y divertida para todo el mundo.

Si estás inscrito, recibirás hoy y mañana sendos correos con información muy útil. Si no estás inscrito, [¡aprovecha hasta las 24h de hoy!](https://forms.gle/wunaG28EcwcvsrE36)

Recordad que el [horario](/schedule) se puede consultar aquí y que tenemos una sección de [preguntas frecuentes](/faq) muy completa.

¡Nos vemos mañana!

