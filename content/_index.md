---
title: Home
icon: home

menu:
  main:
    weight: -1

---


{{% jumbo img="images/backgroundwithoutlogo.jpg" imgLabel="I Meren Palantírion" %}}

## Evento Online de la Sociedad Tolkien Española
# 9-12 Octubre 2020

<!-- <a class="btn primary btn-lg" style="margin-top: 1em;" href="https://drive.google.com/file/d/1td_9Cr1b2JZvv0bCpOCJNDsEWgVgEp2Y/view?usp=sharing" target="_blank">Become a sponsor</a> -->

<!--
<a class="btn primary btn-lg" href="https://conference-hall.io/public/event/HJRThubF4uYPkb7jSUxi">
    <svg class="icon icon-cfp"><use xlink:href="#cfp"></use></svg>Submit a presentation
</a>
-->

{{% /jumbo %}}


{{% home-info  class="primary" %}}
## ¿Qué es la Meren Palantírion?

La Meren Palantírion (“Festival de las Piedras Videntes” en idioma quenya) se concibe como unas jornadas en línea para poder **disfrutar de la obra y el universo Tolkien** desde casa. Aspiramos a generar un espacio de encuentro donde se llevarán a cabo distintas actividades como talleres, charlas, juegos y mesas redondas, entre otras.

Queremos transportar el espíritu del Profesor a través de las plataformas online y aprovechar esta ocasión para invitar a todas aquellas personas que quieran unirse al festival y disfrutar como un ent viajando hacia el sur.

**La PalantirCon concluyó. Ved esta información para disfrutar de los directos grabados >** [Noticia sobre el material grabado](/blog/finpalantircon)

{{% /home-info %}}





<!-- {{< youtube-section link="AFhHrQIAw3g" title="Watch 2019 into" class="" >}} -->



{{% home-speakers %}}
## Con la participación confirmada de todas estas personas

<!--
{{< button-link label="Submit a presentation"
                url="https://conference-hall.io/public/event/HJRThubF4uYPkb7jSUxi"
                icon="cfp" >}}
--> 

{{< button-link label="See all speakers"
                url="./speakers"
                icon="right" >}}


{{% /home-speakers %}}


<!-- ... -->


{{% home-location
    image="/images/tolkien.jpg"
    address="¡Conoce todo sobre la STE!"
    link="https://sociedadtolkien.org"%}}

## La Sociedad Tolkien Española

### Desde 1991 volcados con JRR Tolkien

La STE, con sede en España, es una asociación cultural destinada a promover la obra del profesor JRR Tolkien. Agrupa a una comunidad internacional de más de 500 miembros.

{{% /home-location %}}

