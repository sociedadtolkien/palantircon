---
title: Horario
menu:
  main:
    weight: 10
draft: false
horizontal: false

---

{{% hero %}}

A continuación podéis disfrutar del cuadrante de las actividades para los cuatro días que dura la PalantirCon.

Cada día cuenta con tres o cuatro tracks. Siempre está el principal, que es donde se desarrolla la mayoría de la PalantirCon. Pero en paralelo tenemos un Track alternativo con alguna actividad privada que solo atañe a las personas que están inscritas a ella.

Asimismo, siempre estarán disponibles dos salas virtuales llamadas Una reunión inesperada #1 y #2 para proporcionar encuentros fortuitos entre asistentes a la PalantirCon.

{{% /hero %}}
