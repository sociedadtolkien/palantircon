---
title: Streaming
fullTitle: Plataforma de Streaming
brief: Instrucciones para ponentes y asistentes
type: kids
image: /images/kids/cover.jpg
menu:
  main:
    weight: 90

draft: false
---

Con el streaming o transmisión en vivo se captura un evento a través de vídeo, de tal manera que la audiencia lo disfruta en tiempo real. Al tratarse de un evento online, la Meren Palantírion o PalantirCon tiene que poder resolver la logística asociada a las conexiones de vídeo y audio. Recuerda visitar también la sección de [Preguntas Frecuentes](/faq) en donde ampliamos aspectos concretos del streaming y los directos.


## Clausura de la PalantirCon

Podéis ver en diferido todas las sesiones [en este enlace](https://www.youtube.com/playlist?list=PLrVeBpvB2kh_6BQ-iRzhVIjCSnDbdXudF).
