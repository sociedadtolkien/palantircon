---
title: Sergi Vidal Sánchez
type: gracias
subtitle: Barbarroc
photo: barbarroc.jpg
socials:
  - link: 'https://instagram.com/barbarroccreations'
    name: Instagram
  - link: 'https://facebook.com/Barbarroc/'
    name: Facebook
  - link: 'http://barbarroc.cat'
    name: Site
    

---
