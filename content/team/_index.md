---
title: Organización
type: team
fullTitle: Organización 
brief: El equipo detrás de la Meren Palantírion
image: /images/kids/cover3.jpg
menu:
  main:
    weight: 60

draft: false

---

<!-- ... -->

{{< teams types="core=Equipo Principal,gracias=Agradecimientos" >}}

<!-- ... -->
