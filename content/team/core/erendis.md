---
title: Paula Andrés Martínez
type: core
subtitle: "Erendis"
photo: erendis.jpg
socials:
  - link: 'https://twitter.com/lucreziadeborja'
    name: Twitter
  - link: 'https://www.instagram.com/lucreziadeborja'
    name: Instagram

---

