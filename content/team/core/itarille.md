---
title: Macarena Pingarrón Romero
type: core
subtitle: "Itarillë"
photo: itarille2.jpg
socials:
  - link: 'https://twitter.com/Wasserantian'
    name: Twitter
  - link: 'https://www.instagram.com/macpinga'
    name: Instagram

---

