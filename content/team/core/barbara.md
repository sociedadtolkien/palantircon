---
title: Bárbara García Huertas
type: core
subtitle: "Ar-Feiniel"
photo: barbara.jpg
socials:
  - link: 'https://www.twitter.com/GinebraAquitana'
    name: Twitter
  - link: 'https://instagram.com/ginebra.aquitana'
    name: Instagram

---

