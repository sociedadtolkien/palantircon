---
title: FAQs
fullTitle: Preguntas más frecuentes
brief: Instrucciones para ponentes y asistentes
image: /images/kids/cover2.jpg
type: faqs
menu:
  main:
    weight: 100
    
draft: false
---

## General

### La Meren Palantírion ha concluido ¿dónde están las grabaciones de las actividades?

La PalantirCon se clausuró el 12 de octubre de 2020. Disfrutamos de 4 días (viernes tarde/noche, sábado mañana, tarde y noche, domingo mañna, tarde y noche, lunes mañana y tarde) con más de 30 actividades y más de 50 participantes en directo. Sin embargo, todas las sesiones públicas pueden encontrarse en [esta playlist de Youtube](https://www.youtube.com/playlist?list=PLrVeBpvB2kh_6BQ-iRzhVIjCSnDbdXudF) del canal de la Sociedad Tolkien Española. Se recomienda tener el [Horario](/schedule) a mano para encontrar rápidamente la grabación que os interese. 

### ¿Qué es la Meren Palantírion?

La [Sociedad Tolkien Española](https://www.sociedadtolkien.org), entre sus diferentes eventos, organiza la Mereth Aderthad o EstelCon, el encuentro anual entre todos los miembros de la misma y abierto a cualquier persona que quiera acudir; una Fiesta de la Reunión de varios días de duración llena de conferencias, talleres, música, juegos, lecturas… Sin embargo, por causa de las circunstancias del tiempo que vivimos debido a la actual pandemia del Covid-19, el tan esperado evento de la XXV Mereth Aderthad -que tendría lugar del 9 al 12 de octubre- ha sido suspendido en esta edición. Tras esta dura decisión, el vacío que deja esta celebración anual hasta su próxima realización ha dado paso a una propuesta para que las socios y socios de la STE, y  cualquier persona que quiera asistir, puedan hacerlo desde su hogar: **la Meren Palantírion o PalantirCon**.

La Meren Palantírion (quenya para Festival de las Piedras Videntes) **se concibe como unas jornadas en línea para poder disfrutar de la obra de Tolkien en casa**, en las que se llevarán a cabo talleres, charlas, simposios, juegos y mesas redondas entre otras actividades. Con esta iniciativa queremos llevar el espíritu del Profesor a las plataformas online ya que no podemos reunirnos físicamente, y aprovechar esta ocasión para invitar a toda aquella persona que quiera unirse al festival.


### ¿Quién organiza la Meren Palantírion?

La organización está formada por un grupo de personas voluntarias que, perteneciendo a la Sociedad Tolkien Española, no la representan como tal. No hubo proyecto presentado y votación por la Asamblea General, como sucede con las Merith Aderthad, pero como todas las iniciativas en el seno de la STE que cumplen con sus objetivos, se apoya y difunde con todos sus medios.

Si quieres conocer al equipo detrás del proyecto, puedes verlo en la sección de [Organización](/team).

## Evento

### ¿Cuándo y cómo tendrá lugar?

La Meren Palantírion o PalantirCon tendrá lugar entre los días 9 y 12 de octubre de 2020, desde las 18h del día 9 a las 14h del día 12. Empleamos en nuestras comunicaciones horario central europeo CET (UTC/GTM+1), por lo que las horas están referidas a este huso horario.

Puedes consultar el detalle en la sección de [Horario](/schedule). 

Al ser un evento online, el streaming se realizará a través de YouTube. Será **abierto y gratuito** para todas aquellas personas que quieran escuchar las charlas o simposios, y en el caso de actividades que puedan contar con un aforo limitado (como juegos de rol) se indicará más adelante cómo participar en la página web y las redes sociales del mismo:[Facebook](https://www.facebook.com/merenpalantirion), [Twitter](https://twitter.com/PalantirCon) e [Instagram](https://www.instagram.com/merenpalantirion).

Puedes leer más sobre este aspecto en la sección de [Streaming](/streaming).

### ¿Puedo enviar propuesta de actividad?

Lamentablemente, la recepción de envío de propuestas se cerró el pasado 8 de septiembre de 2020.

### ¿Por qué hay tantas actividades de media hora?

Está comprobado que en eventos online con muchas actividades sucesivas, aquellas que superan los 30 minutos tienen una incidencia muy alta en el agotamiento de las personas asistentes e incluso de las que participan activamente, sobre todo si no son comunicadoras profesionales. Una simple regla establece que, en online, una actividad como una charla se experimenta como si durara el doble en presencial. 

Para la persona que expone, al no poder hacer uso del lenguaje corporal ni tener retroalimentación visual del público, es muy habitual que pueda condensar en menos tiempo una misma charla pensada para presencial. Asimismo, las personas asistentes, en su casa con un ordenador, tienen muchas más oportunidades para distraerse, hecho que se potencia porque la energía de la charla no se experimenta de la misma forma.

Finalmente, reduciendo un poco el tiempo de las actividades, más gente se ha animado a presentar propuesta. Para la Meren Palantírion preferimos la mayor participación posible y estamos felices de ver un porgrama tan diverso.

### ¿Cómo puedo inscribirme?

Para poder asistir de forma libre y gratuita, necesitamos que te inscribas en este formulario. Esto nos permitirá poder informarte puntualmente de cualquier novedad o instrucciones así como enviarte los enlaces correspondientes al streaming.

Para participar en las actividades que requieran inscripción aparte, consulta las instrucciones de cada una de ellas, ya sea consultando el [horario](/schedule) o en el listado general de [charlas](/sessions). Fíjate en las charlas que tienen asociado el icono de **🎫 Inscripción** ya que éstas pueden tener asistencia restringida.

### ¿En qué idioma se van a desarrollar las actividades?

Al ser un evento de la Sociedad Tolkien Española y con la ilusión de abarcar a la comunidad tolkiendil hispanoamericana, el idioma principal será el castellano. No obstante, puede que algunas charlas se desarrollen en otro idioma. En ese caso quedará claramente marcada en la ficha de la actividad. No está prevista interpretación simultánea. 

### ¿Está prevista la grabación de las actividades?

En su mayor parte, las actividades se grabarán para consumo posterior sin límite dentro del canal de YouTube de la Sociedad Tolkien Española. Algunas actividades no se grabarán por tener una naturaleza privada de las personas asistentes. No será hasta el final del evento que podamos confirmar qué grabaciones quedarán finalmente disponibles para el futuro. Os rogamos paciencia y comprensión con esto.

### ¿Hay posibilidad de obtener algún beneficio exclusivo mediante una cantidad de dinero voluntaria?

No está previsto que mediante pago se pueda obtener un servicio o producto especial. No obstante, este evento consume recursos y tiempo de la Sociedad Tolkien Española y te invitamos a consideres hacerte miembro de ella contribuyendo de forma directa a mantener en el futuro estas iniciativas. Tienes toda la información en la sección de [Hacerse socio](https://www.sociedadtolkien.org/hazte-socio/) de la web oficial de la Sociedad Tolkien Española.

## Instrucciones para asistentes y participantes

### Me he inscrito y estoy de asistente ¿dónde me conecto?

La Sociedad Tolkien Española, [a través de su canal de Youtube](https://www.youtube.com/user/STESociedadTolkienES), emitirá la PalantirCon en directo. Algunas actividades de tipo privado quedan excluidas pero la gran mayoría serán de acceso público con un chat para participar. Habrá una persona de la organización que estará pendiente del chat para trasladar algunas preguntas o comentarios a la persona tras la actividad en sí. Os animamos a participar en el chat y a conocer a otros tolkiendili. Más información en la sección de [Streaming](/streaming).

### Me he inscrito pero además participo directamente en una actividad ¿cómo debo hacer?

Salvo para la actividad en la que participes directamente en el directo, sigue las instrucciones de asistente. Ahora bien, para la actividad en donde presentas una charla o taller o si eres parte de una mesa redonda o una actividad especial, entonces es importante que conozcas las instrucciones de conexión y las sugerencias para que todo salga perfecto.

El martes 6 y miércoles 7 estará habilitado desde las 18h hasta las 22h (CET) una sala virtual de pruebas de StreamYard. 
Te llegará un correo con un enlace para el martes y el miércoles, podrían serán diferentes. Accede a ese enlace con un ordenador de sobremesa o portátil y un navegador moderno, preferiblemente Chrome o Firefox. 

Puedes conectarte cuando quieras en esa franja, elige el momento que mejor te vena y reserva 15-20 minutos. Estaremos "de guardia" todo ese periodo los dos días.

Activa la cámara y el micrófono según se te indicará en esa URL y entrarás en una sala de espera donde estaremos pendientes. A partir de ahí haremos una sencilla prueba que durará unos 10-15 minutos para asegurarnos de que todo funciona perfectamente.


{{< centered-img src="/images/streamyard_test.jpg" alt="Aspecto de una prueba preliminar en solitario" width="800" >}}
*Aspecto de una prueba preliminar en solitario en donde la persona tiene la webcam activada y ha compartido una ventana de su ordenador*


### Participo en una partida de rol o una actividad que seguramente sea privada ¿qué plataforma se usa?

Estamos ante el tercer caso que tenemos contemplado. No es un directo en abierto al que asistentes en Youtube y no participas dentro de la actividad en sí vía Streamyard. Para las actividades privadas, hemos puesto a disposición de los propoentes una tercera herramienta por si quisieran usarla, se trata de Bluejeans y funciona con enlaces y el navegador. En este caso, será el organizador de la actividad el que tenga la última palabra y comente con las personas inscritas cómo se desarrollará la actividad.

En todo caso, tendrá que ser algo cómodo y sencillo para todo el mundo.

### Participo directamente en una actividad en donde se me va a ver y escuchar ¿cuáles son los consejos para que todo salga bien?

Si has intervenido antes dando una charla ante mucha gente en un evento online, estos consejos los conocerás. No es lo mismo participar en una videoconferencia con varias personas que impartir una charla, igual que no es lo mismo que soltar un discurso en la barra de un bar que dar una charla en un auditorio.

Sigue estas pautas y el resultado será muchísimo mejor, hay que evitar que todo el trabajo previo de preparación desluzca ante cientos de personas por haber descuidado alguno de estos aspectos.

- Una **buena conexión a Internet** es clave. Al menos 10Mbps. ADSLs y fibra óptica hoy en día ofrecen mucho más que eso pero siempre es bueno recalcarlo.
- Conexión desde un ordenador de **sobremesa o portátil**, no un tablet o móvil. 
- Conexión con **cable** mucho mejor que WIFI. Parece mentira pero todavía hoy un buen equipo y una buena conexión a Internet puede sufrir por un WIFI de poca calidad.
- Una **cámara web** de al menos 720p de resolución que apunte **de forma frontal a tu cabeza y hombros**. Es mejor un encuadre en donde llenes la pantalla que uno en el que se te vea pequeño.
- Si no tienes control sobre el micrófono y los altavoces, existe un riesgo alto de que se produzca acople entre ellos. Mejor siempre **usa cascos o auriculares** salvo que tengas este aspecto totalmente controlado. Además, la calidad del audio se agradece mucho en visionados posteriores de la grabación.
- Evita que detrás de ti haya **un fondo que distraiga** a la audiencia. Puede resultar tentador tener algo curioso detrás de ti pero corres el riesgo de que distraiga una vez perdida la novedad.
- Una buena iluminación. ¡Queremos verte bien! Muchas cámaras web sufren en condiciones de poca luz. El equipo de realización quiere poder jugar con tu cámara y la presentación que estés usando para hacerlo más ameno, ¡pónselo fácil!
- **Evita ruidos y distracciones** en tu entorno desde 15 minutos antes de que empiece tu actividad hasta que ésta haya concluido. Es un directo así que si aparece un gato en medio de la cámara o llaman a la puerta, no se acaba el mundo pero será una pena que tengas que pedir disculpas, perder el hilo y retomarlo cuanto antes.
- **Relájate**, es un directo, pero estás entre amistades y familia tolkiendil :) Eso sí, la mejor forma de rebajar el nivel de nervios es **haber ensayado antes un par de veces cronometrando** y ajustando tiempos si es necesario.

## Más allá de las actividades

### ¿Están previstos otros espacios de encuentro entre las personas asistentes?

Sí. Nos gustaría que al margen de disfrutar de las diversas actividades, las personas que se han acercado e inscrito a la Meren Palantírion puedan tener espacios relajados para encontrarse y charlar. Para ello se habilitarán una serie de salas virtuales específicamente diseñadas para ello. A medida que se acerque la fecha del evento, publicaremos todos los detalles en esta página y la enviaremos a las personas inscritas.