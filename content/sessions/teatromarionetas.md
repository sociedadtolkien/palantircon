---
key: teatromarionetas
title: "Teatro de marionetas: Caperucita roja y el dragón feroz"
id: zVUXQkHQtSiGJeIwVd05
format: especial
tags: null
level: Domingo 11 / 12.30h
speakers:
  - 0alasse_lorien
videoId: 
presentation: null
draft: false
date: 2016
---
Un pequeño teatro para niños con marionetas.