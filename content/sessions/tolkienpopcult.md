---
key: tolkienpopcult
title: "Tolkien Pop-Cult. Presencia de la obra de Tolkien en la Contemporaneidad."
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Lunes 12 / 12.30h
speakers:
  - baldemar_melgar
videoId: 
presentation: null
draft: false
date: 2025
---
Exploración rápida de la presencia de la obra de Tolkien en diferentes disciplinas e intereses humanos contemporáneos.