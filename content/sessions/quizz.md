---
key: quizz
title: "Quizz 'Regreso a Hobbiton'"
id: zVUXQkHQtSiGJeIwVd05
format: especial
tags: null
level: Lunes 12 / 17.00h
speakers:
  - 0elia
  - 0rebeca_perez
  - eleder
  - 0erendis
videoId: 
presentation: null
draft: false
date: 2027
---
¡Diviértete con nosotros y descubre cuánto sabes sobre la obra de Tolkien!

Juego de preguntas y respuestas a través de la plataforma “Quizz” con premios para los ganadores