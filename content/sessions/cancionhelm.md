---
key: cancionhelm
title: "Cancion: La historia de Helm, Manomartillo"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Sábado 10 / 23.15h
speakers:
  - rodrigo
videoId: 
presentation: null
draft: false
date: 2014
---
Nos contará la historia de Helm, El Manomartillo, pesadilla de odontólogos, furia hecha nudillos y pelo de lobos; quien luego de tomar pésimas decisiones políticas, no se doblegó ante la nieve, la tormenta, las lanzas, pero si ante su mujer. Esta es una canción que resuena en todos los confines de este largo país con forma de condimento, desde los fiordos congelados del sur hasta las ardientes arenas del norte
