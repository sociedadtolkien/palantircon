---
key: musica_poeta
title: "Música para un poeta"
id: zVUXQkHQtSiGJeIwVd05
format: charla-larga
tags: null
level: Sábado 10 / 22.30h
speakers:
  - joaquin_ocana
  - toni_lirendil
videoId: 
presentation: null
draft: false
date: 2014
---
Musicalización de los poemas de El Señor de los Anillos. Vamos a presentar el proyecto de poner música a los poemas originales de Tolkien y mostrar algunas de las canciones que ya tenemos. 

Charla propuesta por la Comisión de Música y Bailes (Ainulindalë).