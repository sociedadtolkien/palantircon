---
key: inauguracion
title: "Inauguración de la PalantirCon"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Viernes 9 / 17.00h
speakers: null
videoId: 
presentation: null
draft: false
date: 1999
---
Breve inauguración de la Meren Palantírion, consejos prácticos y repaso de cada día antes de dar paso a la charla inaugural.