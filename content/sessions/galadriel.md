---
key: galadriel
title: "Las palabras de Galadriel. Análisis y traducción del poema Namarië"
id: zVUXQkHQtSiGJeIwVd05
format: especial
tags: 
  - _ins
level: Viernes 9 / 18.00h
speakers:
  - joaquin_ocana
videoId: 
presentation: null
draft: false
date: 2001
---
Análisis de forma interactiva del poema Namárië. La intención es que al finalizar la actividad, los participantes puedan leer partes del poema original entendiendo el significado y morfología del poema. No requiere conocimientos previos de quenya.

La inscripción se requiere para participar interactivamente en la sesión, independientemente de que la actividad tendrá streaming vía Youtube como todas las de carácter público.

**Inscripciones cerradas**


