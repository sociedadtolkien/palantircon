---
key: premios
title: "Entrega de premios de la STE"
id: zVUXQkHQtSiGJeIwVd05
format: premios
tags: null
level: Lunes 12 / 18.00h
speakers: 
    - 0erendis
videoId: 
presentation: null
draft: false
date: 2028
---
Entrega de premios de la STE. Todos los años, la Sociedad Tolkien Española celebra cuatro certámenes. 

Premio ensayo «Ælfwine»: con los objetivos de promover el conocimiento y estudio de la vida y obra de J.R.R. Tolkien y dar a conocer y estimular la elaboración de ensayos y trabajos de investigación de calidad encuadrados en el ámbito de la no-ficción.

Premio relato «Gandalf»: relatos de hasta 15.000 palabras que deben estar ambientados coherentemente en cualquiera de las obras y mundos de Tolkien.

Premio de Arte «Niggle»: en donde se premian obras presentadas atendiendo a diferentes artes pictóricas, ilustración e imagen así como escultura, orfebrería, vestuario y cualquier tipo de obra en tres dimensiones.

Premios microrrelato «Bilbo»: cada tweet o mensaje debe ser un microrrelato en sí mismo. La edición 2020 tiene como tema Númenor.

Durante la ceremonia de entrega de premios se anunciarán los nombres de las personas merecedoras de algún premio, accésit o mención especial y la obra que merece tal distinción.