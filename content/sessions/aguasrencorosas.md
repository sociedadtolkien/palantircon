---
key: aguasrencorosas
title: "Aguas Rencorosas. Partida rol online Cultos Innombrables 4 jugadores"
id: zVUXQkHQtSiGJeIwVd05
format: partida-larga
tags:
  - _ins
level: Sábado 10 / 10.00h
speakers:
  - 0rebeca_perez
videoId: 
presentation: null
draft: false
date: 2005
---
Partida de rol online de unas 4-5 horas para 4 jugadores.

La inscripción se requiere para participar interactivamente en la sesión. Esta actividad no se compartirá vía streaming en Youtube ya que no es de carácter público.

Si deseas participar en la partida, [rellena el siguiente formulario](https://docs.google.com/forms/d/e/1FAIpQLSfc6_O8-qNozBO6xNNQT5xAWI4NJiF_HH4EnTi3sIbiQ0XCcg/viewform).
