---
key: ardarathorn2
title: "Ardarathorn 2.0"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Domingo 11 / 16.30h
speakers:
  - helios_de_rosario
videoId: 
presentation: null
draft: false
date: 2018
---
Se presentará un proyecto en marcha para crear una herramienta online que permita hacer búsquedas automáticas de citas en el sistema de referencias "Arda", a partir de texto libre.