---
key: odiogatos
title: "Tolkien y su odio a los gatos"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Sábado 10 / 16.30h
speakers:
  - eleder
videoId: 
presentation: null
draft: false
date: 2008
---
Quiero tratar el tema de la relación de Tolkien con los gatos, basándome tanto en las referencias a ellos en su obra como en rasgos de su biografía.