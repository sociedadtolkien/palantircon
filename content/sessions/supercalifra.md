---
key: supercalifra
title: Supercalifragilistico espialidoso. J. R. R. Tolkien y Pamela Travers (autora de 'Mary Poppins')
id: 0
language: spanish
format: charla
tags: null
level: Viernes 9 / 17.30h
speakers:
  - 0leonor_ferrandez
videoId: null
presentation: null
draft: false
date: 2000
---
Tolkien y Pamela Travers tuvieron trayectorias semejantes (misma época, similares antecedentes, un gran éxito lierarario, etc.) y sus vidas estuvieron entrelazadas (hay cartas de cada uno refiriendose al otro). La actividad es una charla de unos 45 minutos con presentación de imágenes y (si hay posibilidad) ronda de preguntas,
