---
key: miedoII
title: "El miedo en Tolkien II. Porque en la Tierra Media también se contaban relatos de terror"
id: zVUXQkHQtSiGJeIwVd05
format: especial
tags: null
level: Domingo 11 / 23.00h
speakers:
  - miedospeakers
videoId: 
presentation: null
draft: false
date: 2024
---
Si veías cierta niebla de misterio y oscuridad en las historias de Tolkien; si leyéndolas sentías reminiscencias de siniestras leyendas que ya conocías de fantasmas y seres mitológicos; si crees que la Tierra Media es más un lugar para pasar miedo... entonces ponte el anillo y entra. En la noche del domingo trataremos de conectar fragmentos del Legendarium con historias de terror de nuestra Edad. Unas conocidas, otras más ignotas.