---
key: palantiri
title: "Las Palantíri: una vista a la Tierra Media"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Domingo 11 / 17.00h
speakers:
  - lorenzo_prado
videoId: 
presentation: null
draft: false
date: 2019
---
Usando comparativas con pueblos y culturas de la saga realidad, Charla sobre las Palantíri, su uso, su historia y la repercusión en la Guerra del Anillo.