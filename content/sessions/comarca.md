---
key: comarca
title: "Rompiendo el velo: la Comarca no era una arcadia feliz"
id: zVUXQkHQtSiGJeIwVd05
format: charla-larga
tags: null
level: Sábado 10 / 17.00h
speakers:
  - jose_miranda
videoId: 
presentation: null
draft: false
date: 2009
---
La actividad consistirá en una dinámica charla (incluyendo fogoso debate), donde el interviniente, que ha escrito un libro (más de uno, de hecho), desarrollará ampliamente su teoría conspiranoica sobre El Señor de los Anillos. No es una charla apropiada para estómagos susceptibles. Por el medio, habrá evaluación continua para ver si la gente está atendiendo.

