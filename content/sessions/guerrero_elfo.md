---
key: guerrero_elfo
title: "¿Ha visto pasar a un guerrero elfo? Partida de rol de Dragones y Mazmorras 5°edición"
id: zVUXQkHQtSiGJeIwVd05
format: partida
tags:
  - _ins
level: Domingo 11 / 10.0hh
speakers:
  - francisco_javier
videoId: 
presentation: null
draft: false
date: 2015
---
Se trata de una partida de rol de tono ligero, perfecta para novatos.
Los jugadores se pondran en la piel de una patrulla orca que debe buscar a un espía elfo infiltrado en la tierra de Mordor.
Un oneshot de unas 2-3 horas con personajes pregenerados de nivel 3. 
El tono será ligero ya que los jugadores no tardaran en reconocer el momento en que se ambienta la aventura.

La inscripción se requiere para participar interactivamente en la sesión. Esta actividad no se compartirá vía streaming en Youtube ya que no es de carácter público.

**Inscripciones cerradas**

