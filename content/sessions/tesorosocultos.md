---
key: tesorosocultos
title: "Tesoros Ocultos"
id: zVUXQkHQtSiGJeIwVd05
format: especial
tags: 
  - _ins
  - _ste
level: Lunes 12 / 13.00h
speakers:
  - arafinwe
videoId: 
presentation: null
draft: false
date: 2026
---
"Mi Tesoro"

Presentamos desde nuestros agujeros o mazmorras los mejores tesoros sobre Tolkien conseguidos laboriosamente.

Coordina Jordi Tolra, Arafinwe. Actividad del Smial Lorien.

La inscripción se requiere para participar interactivamente en la sesión, independientemente de que la actividad tendrá streaming vía Youtube como todas las de carácter público.

Si deseas participar en la actividad presentando tu tesoro particular, [rellena el siguiente formulario](https://forms.gle/NTM6eLiZvYU3Yz4T7). La organización se pondrá en contacto contigo tras hacer la selección final para la actividad.

Dependiendo del número de tesoros seleccionados, es posible que esta actividad supere la hora de duración estimada inicialmente.

