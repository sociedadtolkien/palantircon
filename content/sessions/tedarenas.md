---
key: tedarenas
title: "¿A quién le importa Ted Arenas"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Sábado 10 / 17.45h
speakers:
  - joel_mayordomo
videoId: 
presentation: null
draft: false
date: 2010
---
Un exquisito trabajo basado en una investigación filológica, literaria y mitológica, desde la perspectiva más absurda posible. En resumen, una comedia especulativa. ¿A quién le importa Ted Arenas? Nos adentraremos en la mismísima esencia del personaje para responder a esta pregunta. Preparaos para una minuciosa disección del bueno de Ted. Risas garantizadas.
