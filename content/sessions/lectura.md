---
key: lectura
title: "Lectura de cuentos"
id: zVUXQkHQtSiGJeIwVd05
format: especial
tags: 
  - _ins
level: Viernes 9 / 22.00h
speakers:
  - 0kementari
videoId: 
presentation: null
draft: false
date: 2004
---
Lectura de cuentos. Una lectura de breves fragmentos de la obra de JRR Tolkien o que tengan relación directa con ella. Compartiremos un rato escuchando a las voces de las personas que se hayan inscrito previamente como lectoras y que irán turnándose en un orden previamente establecido.

La inscripción se requiere para participar interactivamente en la sesión, independientemente de que la actividad tendrá streaming vía Youtube como todas las de carácter público.

**Inscripciones cerradas**

