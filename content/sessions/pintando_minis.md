---
key: pintando_minis
title: "Pintando Minis"
id: zVUXQkHQtSiGJeIwVd05
format: taller-largo
tags: 
  - _ins
level: Sábado 10 / 11.30h
speakers:
  - joan_gregori
videoId: 
presentation: null
draft: false
date: 2006
---
Charla sobre el pintado de miniaturas de ESDLA mientras pintamos. Mientras enseñamos una forma rápida y sencilla de pintar miniaturas de ESDLA u otros juegos compartiremos trucos y dudas sobre el pintado de miniaturas.

La inscripción se requiere para participar interactivamente en la sesión, independientemente de que la actividad tendrá streaming vía Youtube como todas las de carácter público.

En breve se publicará el formulario para solicitar participar directamente en la actividad en sí.