---
key: gin_nordic
title: "Gin&Nordic: Grupo de lectura de nórdico antiguo"
id: zVUXQkHQtSiGJeIwVd05
format: especial
tags:
  - _ins
level: Domingo 11 / 22.00h
speakers:
  - rafael_pascual
videoId: 
presentation: null
draft: false
date: 2023
---
Leeremos y traduciremos por turnos un fragmento de una de las sagas islandesas que influyeron a J. R. R. Tolkien. Los inscritos se preparán el texto con antelación (para lo que podrán contar con la ayuda de Rafa Pascual). Ésta es una actividad del grupo "Christopher Tolkien" de la Comisión de Lenguas de la STE, inspirada por el "Viking Club" que Tolkien y Gordon fundaron en la Universidad de Leeds. 

La inscripción se requiere para participar interactivamente en la sesión, independientemente de que la actividad tendrá streaming vía Youtube como todas las de carácter público.

**Inscripciones cerradas**