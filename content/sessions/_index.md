---
title: Actividades
id: 99
menu:
  main:
    weight: 12

talkType: "nop"
draft: false    
---

{{% hero %}}

A continuación se presentan todas las actividades por orden cronológico. Todas ellas tienen su ficha ampliada por lo que recomendamos que pinchéis en las que os interesen para conocer más sobre ellas.

En la mayoría se muestra una categoría de la charla. Hacemos mención especial a dos de estas categorías; la de **🎫 Inscripción** y la de **🔒 STE**. La primera indica que la actividad requiere de algún tipo de inscripción o contacto previo con la persona responsable y este contacto debe producirse días antes del evento. La segunda indica que solo podrán participar directamente personas socias de la Sociedad Tolkien Española, aunque el streaming sea público.



{{% /hero %}}

