---
key: professor_tollers
title: "Professor Tollers: Tolkien y la Universidad"
id: zVUXQkHQtSiGJeIwVd05
format: especial
tags: null
level: Domingo 11 / 18.30h
speakers:
  - 0silvia_gutierrez
  - jose_miranda
  - rafael_pascual
videoId: 
presentation: null
draft: false
date: 2022
---
Presentación a tres voces de la actividad académica de Tolkien, recorriendo sus etapas como alumno y profesor en Oxford y en Leeds, sus publicaciones, sus clases, su actividad como tutor, sus distinciones...
