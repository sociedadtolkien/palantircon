---
key: charla_ste
title: "Charla presentación de la Sociedad Tolkien Española"
id: zVUXQkHQtSiGJeIwVd05
format: charla-larga
tags: null
level: Domingo 11 / 13.00h
speakers:
  - 0barbara
videoId: 
presentation: null
draft: false
date: 2017
---
Charla introductoria a la Sociedad Tolkien Española, moderada por Bárbara Ar-Feiniel y con la participación de presidentes y presidentas de algunas delegaciones locales.
Habrá turno de preguntas al final.