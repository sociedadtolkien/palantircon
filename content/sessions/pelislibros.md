---
key: pelislibros
title: "De las pelis a los libros. Experiencia como fans recientes de Tolkien"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Domingo 11 / 17.30h
speakers:
  - antonio_peco
  - juan_angel
videoId: 
presentation: null
draft: false
date: 2020
---
Coloquio sobre la lectura de El Señor de los Anillos por fans de las adaptaciones cinematográficas que han entrado al Universo Tolkien a través de ellas y reflexión del impacto de El Señor de los Anillos en nuestras vidas.