---
key: tesorostolkien
title: "Los tesoros de Tolkien. 'Tolkien Treasures' de Catherine McIlwaine"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Domingo 11 / 18.00h
speakers:
  - litfanmit
videoId: 
presentation: null
draft: false
date: 2021
---
Charla-análisis sobre el libro "Tolkien Treasures" (Bodleian Library, 2018) de Catherine McIlwaine. Explora las ilustraciones originales de Tolkien recogidas en los archivos de la Bodleian Library en Oxford. ¡Espectacular!