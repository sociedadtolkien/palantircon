---
key: arqueria
title: "Arquería en la Tercera Edad: un estudio comparativo"
id: zVUXQkHQtSiGJeIwVd05
format: charla
tags: null
level: Viernes 9 / 19.00h
speakers:
  - pablo_ruiz
videoId: 
presentation: null
draft: false
date: 2003
---
¿Qué tipos de arcos y técnicas se usaban en la Tierra Media durante la Tercera Edad? ¿Está el fandom equivocado? ¿Podemos inferirlo atendiendo a las diferentes culturas de los pueblos de la Tierra Media? ¿Qué dice el canon de la obra de Tolkien? ¿Se contradice el canon con las especulaciones más plausibles?

Usando comparativas con pueblos y culturas de la saga realidad, estableceremos los paralelismos necesarios para establecer con cierto criterio, qué arcos y técnicas habrían usado elfos, enanos, hombres, orcos o hobbits en diferentes regiones. Y veremos si aguantan el filtro del canon de JRR Tolkien.