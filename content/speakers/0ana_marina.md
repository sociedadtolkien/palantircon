---
key: 0ana_marina
name: ANA MARINA
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Lumiel
city: 'España'
photoURL: /images/speakers/ana_marina.jpg
socials:

---
Ana Marina Fernández Alvar, conocida como Lumiel en la STE, entró en el 2019 y es aficionada a Tolkien desde pequeña. Montaraz del Noroeste, trota entre Ourense y Vigo, entre familia y trabajo. La música marca sus pasos, dando voz y armonía como Bardo al ritmo del camino montaraz.