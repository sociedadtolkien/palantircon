---
key: jose_miranda
name: JOSÉ MARÍA MIRANDA BOTO
id: y0wL4Q9ZnxdOvpCvlaejITZcPBq2
feature: false
company: Findegil
city: 'España'
photoURL: /images/speakers/jose_miranda.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/Findegil172'
    name: Findegil172
---
Profesor de Derecho del Trabajo y de la Seguridad Social en la Universidad de Santiago de Compostela. Es doctor en Derecho por la Universidad de Oviedo, con una tesis sobre las competencias de la Comunidad Europea en materia social. Tiene un libro titulado «El
Derecho en Tolkien».