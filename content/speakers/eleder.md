---
key: eleder
name: JOSU GÓMEZ
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Eleder
city: 'España'
photoURL: /images/speakers/eleder.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/Eleder_'
    name: Eleder_

---
Josu Gómez &quot;Eleder&quot; es miembro de la Sociedad Tolkien desde aproximadamente la Segunda Edad. Ha colaborado con ella desde distintas posiciones, como la Comisión Permanente o la Comisión de Lenguas, de la que es cofundador, y es conocido tanto por sus artículos y charlas de temática lingüística como por sus aportaciones al Cancionero de
la STE, con obras en muchas ocasiones de contenido jocoso. Se le puede encontrar actualmente en el podcast de la STE (Regreso a Hobbiton) y en los eventos a los que su apretada agenda como Catedrático de Alto Élfico le permite escaparse, siempre con una OrcoCola en la mano.