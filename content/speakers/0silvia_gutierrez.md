---
key: 0silvia_gutierrez
name: SILVIA GUTIÉRREZ BREGÓN
id: 00wL4Q9ZnxdOvpCvlaejITZcPBq2
feature: false
company: Isilmë
city: 'España'
photoURL: /images/speakers/silvia_gutierrez.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/falispas'
    name: falispas

---
Doctora en Traducción e Interpretación por la Universidad de Granada, con una tesis sobre la competencia intercultural en la formación y profesión del traductor. Es profesora de Lengua Inglesa en la Universidad Isabel I y traductora audiovisual (EN&gt;ES).