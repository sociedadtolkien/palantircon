---
key: joel_mayordomo
name: JOEL MAYORDOMO
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: Beleg Cúthalion
city: 'España'
photoURL: /images/speakers/joel_mayordomo.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/AsesVanes'
    name: AsesVanes
  - icon: instagram
    link: 'https://instagram.com/uncanny.__'
    name: uncanny.__

---
Joel Mayordomo (Madrid, 2002), también llamado Beleg Cúthalion. Estudiante del grado **Estudios de Asia y África: Japonés** y presidente del Smial de Númenor. Llevo unido a Tolkien toda mi vida gracias a mi padre, que me lo inculcó desde pequeño. Me gusta la
mitología en general, especialmente la nórdica, las lenguas y las drag queens. Por fin os resolveré esa duda que os lleva quitando el sueño toda vuestra vida.