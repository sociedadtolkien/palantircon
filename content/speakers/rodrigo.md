---
key: rodrigo
name: RODRIGO VÁSQUEZ
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Aranruth
city: 'Chile'
photoURL: /images/speakers/aranruth.jpg
socials:
  - icon: instagram
    link: 'https://www.instagram.com/aisling_chile/'
    name: aisling_chile
  - icon: site
    link: http://gensgoliae.com
    name: Gens Goliae

---
De él se dice que es un bardo. También que es un one hit wonder. Fundador de Ohtaríma, Sociedad Tolkien Chilena en Concepción, recorre los caminos de varios mundos buscando historias para contar. Actualmente magister del Gremio de los Bardos de Ohtaríma, guitarrista y cantante en Aisling (https://www.instagram.com/aisling_chile/) y Gens Goliae (http://gensgoliae.com), se dedica a tejer en canciones los cuentos que no deben olvidarse.
