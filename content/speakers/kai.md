---
key: kai
name: BERNARD TORELLO
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Thingol (Kai)
city: 'España'
photoURL: /images/speakers/kai.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/bernardtorello'
    name: kai
  - icon: youtube
    link: 'https://www.youtube.com/c/Kai47'
    name: Kai47

---
Lleva más de cuatro años dedicándose a divulgar la obra de Tolkien a través de Youtube y Redes Sociales, lo que le ha convertido en la persona de referencia en castellano para amantes de la obra del profesor en dicho ámbito. En su canal además se pueden encontrar vídeos sobre el universo Harry Potter, Dragon Ball, videojuegos, etc.