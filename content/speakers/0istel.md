---
key: 0istel
name: ESTER CUENCA LUMBRERAS
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: "Istel"
city: "España"
photoURL: /images/team/istel.jpg
socials:
  - icon: twitter
    link: 'https://www.twitter.com/istel_'
    name: istel_
  - icon: instagram
    link: 'https://instagram.com/istel_'
    name: istel_

---

Ester Cuenca Lumbreras, más conocida como Istel. Soy secretaria de la Sociedad Tolkien España, amiga de elfos, hobbits y estoy prometida con un enano. Cuando era pequeña me caí en una marmita de frikismo y así estoy hoy en día. Me contrataron como enfermera de Mordor o Madrid como le gusta decir a algunos y en eso estoy. He escrito en algunos blogs, hice un prólogo y a veces firmo libros antes de que me pillen. Leí la boda roja antes de que fuese mainstream.

Me gustan las cosas sencillas: los libros, las agujas, las jeringuillas, los cohetes, los cómics, las series y las guaridas del mal. También cocino, y de momento no he envenenado a nadie. Hago la croqueta en twitter y en directo a cambio de vino. A veces bebo otras cosas. Pero más vino. Considero el Hobbit mi libro preferido y hasta tengo la piedra del arca por casa, soy un poco dragona. También tengo un Chewbacca en casa. Y sé usarlo.