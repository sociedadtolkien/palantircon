---
key: pablo_ruiz
name: PABLO RUIZ MÚZQUIZ
id: y0wL4Q9ZnxdOvpCvlaejITZcPBq2
feature: false
company: Aranarth
city: 'España'
photoURL: /images/speakers/aranarth.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/diacritica'
    name: diacritica
  - icon: site
    link: 'https://aljaba.net'
    name: aljaba.net
---
Miembro de la STE desde 1995, fundó el Smial de Númenor primero y el de Hammo después, del que es Presidente en la actualidad. Ha mantenido una actividad sostenida tanto en aspectos organizativos en Merith y Estelcones o la Página Web de la STE, como más creativos o lúdicos, impartiendo numerosas charlas y talleres o componiendo canciones y poemas. Su interés los últimos años por la arquería tradicional e histórica le ha llevado a volver a la obra de JRR Tolkien con un ojo arquero que resuelva el camino más plausible y consistente para el uso del arco y la flecha en los pueblos de la Tercera Edad.

Sus intereses son la ciencia, los derechos civiles, el feminismo, el software libre, la fantasía, la ciencia ficción, la arquería tradicional, los juegos de rol, la fotografía, la programación, el viajar y el escribir.