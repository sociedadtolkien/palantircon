---
key: 0kementari
name: RAQUEL BAEZA
id: 0yzdv6FVFYZWXmWird77HqsY49c2
feature: false
company: Kementári
city: 'España'
photoURL: /images/speakers/baeza.jpg
socials:

---
Amante de la naturaleza y una buena lectura, siempre dispuesta a imaginar una buena
historia y vivirla.