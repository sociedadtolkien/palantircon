---
key: rafael_pascual
name: RAFAEL PASCUAL
id: GMZkedB6bYXQ2J5rJ9aIodIyXV92
feature: false
company: Caradhras
city: 'Reino Unido'
photoURL: /images/speakers/rafael_pascual.jpg
socials:
---
Profesor de literatura inglesa antigua en Magdalen College, Oxford, e investigador en CLASP (A Consolidated Library of Anglo-Saxon Poetry), un proyecto asociado a la Cátedra Rawlinson and Bosworth de Anglosajón. Obtuvo su doctorado en Filología Inglesa por la Universidad de Granada en 2014, con una tesis sobre la datación y la crítica textual de *Beowulf*, en virtud de la cual consiguió un contrato postdoctoral en la Universidad de Harvard.