---
key: helios_de_rosario
name: HELIOS DE ROSARIO MARTÍNEZ
id: J2kgJ25uKngYv9G4PSWj0Cw9fcA3
feature: false
company: Imrahil
city: 'España'
photoURL: /images/speakers/helios_de_rosario.jpg
socials:

---
Miembro de la STE desde 1998, presidente de la misma entre 2001 y 2003, y actualmente presidente de su delegación en Valencia (Smial de Edhellond). Colaborador habitual de la revista &quot;Estel&quot; como autor y revisor de artículos. Ha participado también en charlas
divulgativas sobre J.R.R. Tolkien y su obra en múltiples convenciones y conferencias dedicadas a este autor, organizadas por la STE y Sociedades Tolkien de otros países (Italia, Reino Unido, Alemania), en la Conferencia sobre lenguas inventadas por Tolkien «Omentielva» (España, Reino Unido y EE. UU.), y en jornadas universitarias sobre
literatura, lingüística, geografía e historia (Universitat de València, Univ. Pompeu Fabra de Barcelona, Universitat de les Illes Balears, Univ. Complutense de Madrid y la Universidad de Granada, entre otras).