---
key: 0elia
name: ELIA
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Míriel
city: 'España'
photoURL: /images/speakers/elia.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/_eliamartell'
    name: _eliamartell
  - icon: site
    link: 'http://www.ivoox.com/p_sq_f1153534_1.html'
    name: Regreso a Hobbiton
  - icon: youtube
    link: 'https://www.youtube.com/c/MagosyMedianos'
    name: Magos y Medianos

---
Directora y creadora del podcast de la Sociedad Tolkien Española desde 2015, acaba de dar el salto a Yotube con un canal dedicado a la obra de Tolkien y a la de JK Rowling.