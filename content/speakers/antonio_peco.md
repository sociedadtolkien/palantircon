---
key: antonio_peco
name: ANTONIO PECO
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: 
city: 'España'
photoURL: /images/speakers/antonio_peco.jpg
socials:

---
Mi nombre es Antonio y hace apenas un año que conozco este maravilloso mundo creado por Tolkien. Vi las películas después de que me las recomendaran mil veces y me marcaron de forma especial. Ahora estoy inmerso junto a mis amigos en el siguiente paso, la lectura de los libros.