---
key: 0leonor_ferrandez
name: LEONOR FERRÁNDEZ ALBEROLA
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: Elanor
city: 'España'
photoURL: /images/speakers/leonor_fernandez.jpg
socials:
  - icon: instagram
    link: 'https://instagram.com/ferrandez_alberola'
    name: ferrandez_alberola
---
Llevo vinculada a la obra de Tolkien y a la STE antes incluso de ser consciente de ello. Con unos meses de vida acudí a mi primera Estelcon y desde entonces he participado en casi todas las que se han celebrado. En un principio mi familia fue responsable de ello, pero ahora soy una entusiasta participante por mí misma. Además he tenido ocasión de conocer a importantes personajes vinculados con Tolkien, aunque lo más importante es que ya me planteo mis propios retos e investigaciones.

Mi primera charla en una Estelcon fue la titulada “Fotografiando a Tolkien”. Ahora me planteo profundizar en los aspectos en común que comparten Tolkien y P.L. Travers (la autora de Mary Poppins). Los resultados seguro que sorprenderán a más de uno.