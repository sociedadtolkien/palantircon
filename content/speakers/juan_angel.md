---
key: juan_angel
name: JUAN ÁNGEL LAGUNA
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: 
city: 'España'
photoURL: /images/speakers/juan_angel.jpg
socials:

---
Conozco la Tierra Media desde pequeño a raíz de ver las películas con mi padre. He crecido rodeado de merchandising de El Señor de los Anillos, pero no empecé a leer los libros hasta este verano, cuando un grupo de amigos decidimos dar el salto y formar un pequeño club de lectura.