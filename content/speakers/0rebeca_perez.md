---
key: 0rebeca_perez
name: REBECA PÉREZ
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: Nai
city: 'España'
photoURL: /images/speakers/rebeca_perez.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/rebeca_nai'
    name: rebeca_nai
  - icon: instagram
    link: 'https://instagram.com/rebeca_nai'
    name: rebeca_nai
---
36 años de experiencia en hacer el friki. Entre sus aficiones destacan leer comics, hacer escape rooms, hacer el cabra y traer mundos fantásticos, horribles, oscuros y / o misteriosos al mundanal ruido del día a día.