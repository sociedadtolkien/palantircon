---
key: maglor
name: MAGLOR
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Maglor
city: 'España'
photoURL: /images/speakers/maglor.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/M4GLoR'
    name: M4GLoR
  - icon: youtube
    link: 'https://www.youtube.com/c/Maglor'
    name: Maglor

---
Creador de contenido en YouTube desde hace dos años, especializado en la Tierra Media, Canción de Hielo y Fuego y series como Dark o Westworld.