---
key: toni_lirendil
name: TONI
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Lirendil
city: 'España'
photoURL: /images/speakers/toni_lirendil.jpg
socials:
---
Toni Lirendil es un Bardo valenciano que, como indica su nombre, ama el canto y las canciones. Se dedica a la enseñanza y la dirección musical. Se unió a la STE en la primavera de 2020 aunque su relación con Tolkien se remonte a su infancia, cuando su tío le contaba historias de Hobbits y dragones.