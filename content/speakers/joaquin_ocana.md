---
key: joaquin_ocana
name: JOAQUÍN OCAÑA BRETONES
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: Mardil-Voronwë
city: 'España'
photoURL: /images/speakers/joaquin_ocana.jpg
socials:

---
Soy Joaquín Ocaña Bretones, “Mardil-Voronwë”, licenciado en Traducción y amante de la obra de Tolkien hace muchos años. Me interesan las lenguas en general, las vivas, las muertas y las inventadas. Junto a esto me interesan temas relacionados con el folklore, músicas tradicionales, cuentos, leyendas y mitos de todas las culturas. En estos momentos resido en Madrid y formo parte del Smial de Númenor. Además presido la recién reabierta Comisión de Lenguas de la STE.