---
key: miedospeakers
name: ERIN Æ./ ELEIN / ANGELA / ERADDIEL
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: 
city: 'España'
photoURL: /images/speakers/miedo.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/Elein_88'
    name: Elein_88
  - icon: site
    link: 'https://deviantart.com/arienmaiden'
    name: arienmaiden
  - icon: facebook
    link: 'https://facebook.com/Erinia.Aelia'
    name: Erinia.Aelia
  - icon: facebook
    link: 'https://facebook.com/angiegiadelli'
    name: angiegiadelli

---
Nuestro primer contacto con la obra de Tolkien se produjo durante la adolescencia, primero con sus libros y luego con la trilogía cinematográfica. Esta afición por el profesor nos llevó a diversas plataformas de ficciones y allí surgió una amistad en la que dimos rienda suelta a nuestra pasión por el universo de Eru.

En la EstelCon 2016 participamos con dos actividades, una divulgativa de la influencia de Tolkien en el mundo de las fanficciones, y otra para demostrar que la Tierra Media en realidad es un lugar donde se pasa mucho miedo. Ahora volvemos para adentrarnos en la oscuridad de Arda, que se extiende desde Mordor hasta los lugares que menos cabría
esperar, sin saber muy bien si saldremos.