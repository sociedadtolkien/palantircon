---
key: 0elena_gwainiel
name: ELENA SISO
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Gwainel
city: 'España'
photoURL: /images/speakers/elena_gwainiel.jpg
socials:

---
Elena &quot;Gwainel&quot; del Smial de Númenor. Asturiana de nacimiento y madrileña de adopción.
Hija de la Tierra Media, amante de los juegos de cartas y de rol.