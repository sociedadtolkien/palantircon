---
key: joan_gregori
name: JOAN GREGORI BAGUR
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Ancalagon el Negro
city: 'España'
photoURL: /images/speakers/joan_gregori.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/gregorinagur'
    name: gregoribagur
  - icon: instagram
    link: 'https://instagram.com/dracdargent'
    name: dracdargent

---
Soy Ancalagon el negro, socio de la STE desde 2007 y jugador de miniaturas desde que Sauron era cadete. Soy miembro de la comisión de juegos y fueron los juegos lo primero que me vinculó al mundo de Tolkien con el mítico juego de rol de Joc Internacional. 

En twitter podéis encontrarme como @gregoribagur y en instagram dónde suelo colgar fotografías de algunas miniaturas y de cafés acabados como @dracdargent