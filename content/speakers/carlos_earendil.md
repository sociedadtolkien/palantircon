---
key: carlos_earendil
name: CARLOS
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: Eärendil
city: 'España'
photoURL: /images/speakers/carlos_earendil.jpg
socials:

---
Carlos Eärendil, capitán de los Montaraces y bardo errante. Enamorado del camino, de las canciones y de las personas. Durante la Meren Palantirion llevaré a cabo el VingiloTour recorriendo media España y reuniendo a montaraces junto al fuego para contar historias.