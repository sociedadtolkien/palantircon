---
key: javi_marcos
name: JAVIER MARCOS
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Dúnadan
city: 'España'
photoURL: /images/speakers/javi_marcos.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/javimgol'
    name: javimgol
  - icon: site
    link: 'https://www.ivoox.com/podcast-cancion-continua_sq_f1875597_1.html'
    name: La Canción Continúa
  - icon: youtube
    link: 'https://www.youtube.com/c/javimgol'
    name: Javi Marcos

---
Youtuber, podcaster y administrador de la web [lossietereinos.com](https://lossietereinos.com). Si bien en Redes Sociales se le conoce por su dominio de la obra de George RR Martin, sus conocimientos de Tolkien son igualmente extensos y profundos.