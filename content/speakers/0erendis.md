---
key: 0erendis
name: PAULA ANDRÉS MARTÍNEZ
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: "Erendis"
city: "España"
photoURL: /images/team/erendis.jpg
socials:
  - icon: twitter
    link: 'https://www.twitter.com/lucreziadeborja'
    name: lucreziadeborja
  - icon: instagram
    link: 'https://instagram.com/lucreziadeborja'
    name: lucreziadeborja

---

Miembro de la STE desde 2003. En 2005 me hice miembro del smial de Númenor, desde donde he ido tratando de compartir Tolkien lo mejor que he sabido... y es que gracias a él conocí a mi mejor amiga y compañera de aventuras, quien me arrastró a su último proyecto: Regreso a Hobbiton. Desde ese pequeño agujero Hobbit difundimos al Profesor lo mejor que podemos, disfrutando como enanas y aprendiendo como elfas...

En ocasiones he sido Jurado de diversos premios de la STE y desde hace unos años coordinó los premios Bilbo de micro-relató.

Y es que cuando Tolkien o la STE están por medio, me es  imposible no terminar metida en un lío nuevo... aunque siempre me digo que será el ultimo... pero siempre sé que no es cierto.