---
key: 0monica_moya
name: MÓNICA MOYA HIDALGO
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Smoug Mho
city: 'España'
photoURL: /images/speakers/mho.jpg
socials:
  - icon: instagram
    link: 'https://instagram.com/mhomonica'
    name: mhomonica

---
Profesora de electricidad en formación profesional y aficionada a la obra de Tolkien. Socia de la STE desde 2017. En la EstelCon de Zaragoza de 2016 pude compaginar el universo Tolkien con mi otra gran afición: el Scrapbook. Ambientando mis trabajos en la Tierra Media he impartido talleres en varias ediciones de la Mereth Aderthad. Actualmente caminando junto a montaraces de toda España en el Smial Montaraz.
