---
key: 0alasse_lorien
name: ALASSË DE LÓRIEN
id: 0yzdv6FVFYZWXmWird77HqsY49c2
feature: false
company: 
city: 'España'
photoURL: /images/speakers/martina_morell.jpg
socials:

---
Miembro del Smial de Lorien desde el 2012, aunque se me ha visto poco en eventos, por temas familiares y de trabajo. Siempre que puedo hago activiades para los más pequeños, que suelen ser los fans más agradecidos. De adscripción élfica, que tienen los vestidos más
&quot;fashion víctim&quot;, por supuesto. Vivo en la provincia de Tarragona, con mi marido, también miembro de Lorien y mi hijo de cinco años, negándose empecinadamente ambos a ser elfos.