---
key: arafinwe
name: JORDI TOLRÀ
id: y0wL4Q9ZnxdOvpCvlaejITZcPBq2
feature: false
company: Arafinwe
city: 'España'
photoURL: /images/speakers/arafinwe.jpg
socials:

---
Jordi Tolrà, Arafinwe, es socio de la STE desde el año 2000. Organizó en la UAB el primer curso universitario con créditos académicos edicado a la obra de Tolkien, en el que participaron personalidades como el traductor y editor de ESLA al castellano Francisco Porrúa y Francesc Parcerisas y Dolors Udina, traductores al catalán. Posteriormente, durante el Año del Libro y de la Gastronomía en Catalunya organizó el 50 aniversario de la publicación de El Hobbit en el Mercado de La Boqueria en Barcelona. Miembro del Smial de Lorien. 