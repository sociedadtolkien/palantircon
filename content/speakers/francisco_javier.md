---
key: francisco_javier
name: FCO JAVIER RIVERA-PINNA GARCÍA
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Arahad
city: 'España'
photoURL: /images/speakers/francisco_javier.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/JavaRivP'
    name: JavaRivP
  - icon: facebook
    link: 'https://facebook.com/javierk014'
    name: Javier Rivera-Pinna García

---
Opositor a Judicatura seguidor de la obra de Tolkien desde que recuerdo, llevo un año en la STE, a raiz de la Estelcon de 2019. Miembro del Smial Montaraz actualmente llevo una campaña de rol para varios miembros del mismo.