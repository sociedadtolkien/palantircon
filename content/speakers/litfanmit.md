---
key: litfanmit
name: Cátedra LITFANMIT
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: 
city: 'Internet'
photoURL: /images/speakers/litfanmit.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/CatedrLitFanMit'
    name: CatedrLitFanMit 
  - icon: twitter
    link: 'https://twitter.com/elMitografo'
    name: elMitografo
  - icon: site
    link: 'https://catedralitfanmit.wordpress.com'
    name: catedralitfanmit.wordpress.com

---
La Cátedra LITFANMIT es un espacio de estudio y difusión del género fantástico.
En nuestra cuenta de Twitter y weblog compartimos nuestra pasión por el género fantástico a través de artículos, entrevistas, reseñas y relatos  inéditos en español.

La persona encargada de impartir la charla será nuestro coordinador, Héctor V. D. Asejo (TW: @elMitografo ).

{{< centered-img src="/images/speakers/hector_asenjo.jpg" alt="Héctor Asejo">}}