---
key: baldemar_melgar
name: BALDEMAR MELGAR TAPIAS
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: 
city: 'Bolivia'
photoURL: /images/speakers/baldemar_melgar.jpg
socials:


---
Baldemar Melgar Tapias es un Tolkiendil de las tierras bajas de Bolivia. Nacido en medio de la Amazonía, dedicó desde pequeño sus ratos libres a leer, escribir, los videojuegos y ocasionalmente el teatro, hasta que en su adolescencia encontró los trabajos del Profesor y no paró de explorarlos. Mudándose a los Valles bolivianos, para cursar
Filosofía en la universidad, meta que no llegó a cumplirse, conoció a otros Tolkiendili y llegó a liderarlos como Presidente y Administrador de la Sociedad Tolkien Bolivia durante 3 gestiones consecutivas.