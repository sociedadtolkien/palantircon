---
key: pau_m_just
name: PAU M JUST
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: null
city: 'España'
photoURL: /images/speakers/pau.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/paumjust'
    name: paumjust
  - icon: instagram
    link: 'https://instagram.com/paumjust'
    name: paumjust

---
Arquitecto de Barcelona y divulgador en el canal “Deconstruyendo el Cine” dedicado a explicar la arquitectura y arte de películas y series. Evidentemente sobre El Señor de los Anillos pero también Juego de Tronos, Harry Potter y hasta El Rey León!