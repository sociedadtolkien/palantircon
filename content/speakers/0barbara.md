---
key: 0barbara
name: BÁRBARA GARCÍA HUERTAS
id: fEGtvUISb6VrUv4hpaHxRnw8hFZ2
feature: false
company: "Ar-Feiniel"
city: "España"
photoURL: /images/team/barbara.jpg
socials:
  - icon: twitter
    link: 'https://www.twitter.com/GinebraAquitana'
    name: GinebraAquitana
  - icon: instagram
    link: 'https://instagram.com/ginebra.aquitana'
    name: ginebra.aquitana

---

Vicepresidenta de la STE. Soy historiadora y me especialicé en mujeres en la Edad Media. Conocí a Tolkien con diez años y desde ese momento me ha guiado en mi camino. También soy catadora de croquetas de queso a la cerveza.