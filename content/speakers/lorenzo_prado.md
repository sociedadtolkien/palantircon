---
key: lorenzo_prado
name: LORENZO PRADO RODRÍGUEZ
id: 05FXSY1pHlaP7wK14yGKBaxom6n1
feature: false
company: Tar-Palantir
city: 'España'
photoURL: /images/speakers/lorenzo_prado.jpg
socials:
  - icon: twitter
    link: 'https://twitter.com/lopra_12'
    name: lopra_12
  - icon: facebook
    link: 'https://facebook.com/lopra_12'
    name: Lorenzo Prado Rodríguez
  - icon: instagram
    link: 'https://instagram.com/lopra12'
    name: lopra12

---
Soy Lorenzo Prado Rodríguez, natural del Madrigalejo (Cáceres). En la Sociedad Tolkien Española se me conoce como &quot;Tar-Palantir&quot;, del Smial de Númenor. Soy estudiante de historia en Madrid que intenta aprender cada día más de la vida y la obra del Profesor.